﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task8
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 1, days = 0, temp = 0, hotDays = 0, ottep = 0;

            Console.WriteLine("Vvedite kol-vo dney: ");
            days = int.Parse(Console.ReadLine());

            while (i <= days)
            {
                Console.WriteLine($"Vvedite temperaturu v {i} den: ");
                temp = int.Parse(Console.ReadLine());

                if (temp > 0)
                {
                    hotDays++;
                }
                else
                {
                    hotDays = 0;
                }
                if (ottep < hotDays)
                {
                    ottep++;
                }
                i++;
            }
            Console.WriteLine($" Samaya prodolzhitelnaya ottepel {ottep} dnya");
            Console.ReadKey();
        }
    }
}
