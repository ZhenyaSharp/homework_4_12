﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int inNum, i=0, a,  num;

            Console.WriteLine("Input number: ");
            inNum = int.Parse(Console.ReadLine());

            Console.WriteLine("Input a: ");
            a = int.Parse(Console.ReadLine());

            while (inNum!=0)
            {
                num=inNum % 10;
                inNum = inNum / 10;
                if(num==a)
                {
                    i++;
                }               
            }
            Console.WriteLine($"{a} vstrechaetsya {i} raz");
            Console.ReadKey();
        }
    }
}
