﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            int num, digit, maxDig = 0, minDig = 9, razn, number=0;

            Console.Write("Input number:");
            num = int.Parse(Console.ReadLine());

            while (num != 0)
            {
                digit = num % 10;
                num = num / 10;

                if (maxDig < digit)
                {
                    maxDig = digit;
                }

               if (minDig > digit)
                {
                    minDig = digit;
                }
            }
            razn = maxDig - minDig;

            if (razn % 2 == 0)
            {
                Console.WriteLine("raznica max i min chetnaya");
            }
            else
            {
                Console.WriteLine("raznica max i min nechetnaya");
            }
            Console.ReadKey();
        }
    }
}
