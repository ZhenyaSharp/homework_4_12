﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0, kolvo, i = 0, maxNum = 0, minNum = 2147483647;

            Console.WriteLine("skolko chisel hotite vvesti: ");
            kolvo = int.Parse(Console.ReadLine());

            while (i < kolvo)
            {
                Console.WriteLine("Input number:");
                num = int.Parse(Console.ReadLine());

                if (maxNum < num)
                {
                    maxNum = num;
                }
                if (minNum > num)
                {
                    minNum = num;
                }
                i++;
            }
            Console.WriteLine($"Max Number = {maxNum}, minimal number = {minNum}");
            Console.ReadKey();
        }

    }
}
