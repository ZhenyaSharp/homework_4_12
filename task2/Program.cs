﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num, sum=0;

            Console.Write("Input number:");
            num = int.Parse(Console.ReadLine());

            while (num != 0)
            {
                sum += num % 10;
                num = num / 10;
            }

            if (sum>10)
            {
                Console.WriteLine("True. Summ > 10");
            }
            else if(sum<10)
            {
                Console.WriteLine("False. Summ < 10");
            }
            else
            {
                Console.WriteLine("Summ = 10");
            }

            Console.ReadKey();
        }
    }
}
