﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int inNum, i = 0, num;

            Console.WriteLine("Input number: ");
            inNum = int.Parse(Console.ReadLine());

            while (inNum != 0)
            {
                num = inNum % 10;
                inNum = inNum / 10;

                if (num == 3)
                {
                    i++;                    
                }                
            }
            if (i != 0)
            {
                Console.WriteLine($" 3 vstrechaetsya {i} raz");
            }

            if (i == 0)
            {
                Console.WriteLine("3 ne vstrechaetsa v etom chisle");
            }
            Console.ReadKey();
        }
    }
}
